xnnpack (0.0~git20220216.ae108ef-2+apertis0) electronica2022; urgency=medium

  * Import from Debian bookworm.
  * Set component to target.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Fri, 21 Oct 2022 09:02:17 +0000

xnnpack (0.0~git20220216.ae108ef-2) unstable; urgency=medium

  * Upload to unstable.

 -- Mo Zhou <lumin@debian.org>  Sat, 25 Jun 2022 20:11:28 -0700

xnnpack (0.0~git20220216.ae108ef-1) experimental; urgency=medium

  * New upstream version 0.0~git20220216.ae108ef
    Please pin to this snapshot for PyTorch/v1.12.0~rc1
  * Remove already merged patches.
  * Rebase soversion.patch
  * Resolve superfluous-file-pattern in d/copyright
  * Put binary packages to correct Sections.
  * Add changelog entry for the 0.0~git20201221.e1ffe15-2.1 NMU.
    Many thanks to Adrian Bunk!
  * Upload to experimental.

 -- Mo Zhou <lumin@debian.org>  Sun, 22 May 2022 17:56:02 -0400

xnnpack (0.0~git20201221.e1ffe15-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add upstream fix for FTBFS with gcc 11. (Closes: #997271)

 -- Adrian Bunk <bunk@debian.org>  Tue, 28 Dec 2021 20:29:08 +0200

xnnpack (0.0~git20201221.e1ffe15-2) unstable; urgency=medium

  * Upload to unstable.

 -- Mo Zhou <lumin@debian.org>  Thu, 02 Sep 2021 02:12:54 +0000

xnnpack (0.0~git20201221.e1ffe15-1) experimental; urgency=medium

  * New upstream version 0.0~git20201221.e1ffe15
    Please pin to this snapshot for PyTorch/v1.8.1
  * Remove 533.patch (merged upstream) and refresh remaining patches.
  * Add PyTorch/v1.8.1 upstream patches.

 -- Mo Zhou <lumin@debian.org>  Wed, 19 May 2021 13:02:03 +0800

xnnpack (0.0~git20201031.beca652+really.git20200323.1b35463-2) unstable; urgency=medium

  * Don't install unwanted header files.
  * Revert "Remove 533.patch (merged upstream) and refresh the remaining ones."
    We need this patch again due to the rollback.

 -- Mo Zhou <lumin@debian.org>  Fri, 11 Dec 2020 13:50:30 +0800

xnnpack (0.0~git20201031.beca652+really.git20200323.1b35463-1) unstable; urgency=medium

  * Rollback to upstream snapshot 0.0~git20200323.1b35463
  * Please pin to this snapshot for PyTorch 1.7.0
  * Rebase existing patches.
  * Stop tracking symbols, and update not-installed list.

 -- Mo Zhou <lumin@debian.org>  Fri, 11 Dec 2020 12:33:46 +0800

xnnpack (0.0~git20201031.beca652-1) unstable; urgency=medium

  * New upstream snapshot 0.0~git20201031.beca652
  * Change maintainer into Debian Deep Learning Team <debian-ai@l.d.o>.
  * Refresh symbols list for amd64 architecture.

 -- Mo Zhou <lumin@debian.org>  Mon, 02 Nov 2020 16:02:16 +0800

xnnpack (0.0~git20200827.37297a6-1) unstable; urgency=medium

  * New upstream version 0.0~git20200827.37297a6
  * Remove 533.patch (merged upstream) and refresh the remaining ones.
  * Refresh symbols control file.

 -- Mo Zhou <lumin@debian.org>  Thu, 03 Sep 2020 10:37:10 +0800

xnnpack (0.0~git20200425.54f5917-4) unstable; urgency=medium

  * Strip and embedded clog and remove the corresponding patch.
  * Bump debhelper compat level to 13.
  * Remove README.Source, as the embedded clog has been removed.

 -- Mo Zhou <lumin@debian.org>  Fri, 01 May 2020 14:11:14 +0800

xnnpack (0.0~git20200425.54f5917-3) unstable; urgency=medium

  * Make the symbols control file amd64-only.
  * Override dh_dwz and allow it to fail on arm64.

 -- Mo Zhou <lumin@debian.org>  Thu, 30 Apr 2020 11:42:31 +0800

xnnpack (0.0~git20200425.54f5917-2) unstable; urgency=medium

  * Fixup the clog patch (it was empty in the previous upload due to mistake)
  * Apply wrap-and-srot.
  * Create symbols control file for libXNNPACK.so
  * List not-installed files.
  * Upload to unstable.

 -- Mo Zhou <lumin@debian.org>  Wed, 29 Apr 2020 09:27:23 +0800

xnnpack (0.0~git20200425.54f5917-1) experimental; urgency=medium

  * Initial release. (Closes: #958818)

 -- Mo Zhou <lumin@debian.org>  Sun, 26 Apr 2020 15:04:57 +0800
